<?php
/**
 * @file
 * uams_colleges_schools_and_majors_listings.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function uams_colleges_schools_and_majors_listings_taxonomy_default_vocabularies() {
  return array(
    'uams_alphabet_list_filter' => array(
      'name' => 'Alphabet list filter',
      'machine_name' => 'uams_alphabet_list_filter',
      'description' => 'A-Z so we can tag certain entities.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
