<?php
/**
 * @file
 * uams_colleges_schools_and_majors_listings.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function uams_colleges_schools_and_majors_listings_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'K',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => '04f0604a-1556-4648-8e2a-5c5c1e2ef5df',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'M',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => '06092846-7e68-4dc3-9355-5303ea8ee1df',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'B',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => '08e7fe11-96b9-46d1-8836-b9788cfae27e',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'G',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => '0c5b8d04-897e-48b3-8647-bf78c53de823',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'E',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => '1956747e-8386-4ce5-ab92-e12d57a3751f',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'F',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => '269a5db3-d743-421c-9931-499c38f16e3c',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'V',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => '2c0997c6-9198-4172-a784-959b5c9b0b78',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'X',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => '3981f778-c3c8-47fa-adb2-37d786571432',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'C',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => '3ac7812e-f92e-4e35-9cb1-9d9cdca7562e',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'A',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => '3ee14745-81ab-420c-8c9b-dc530b5192ad',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Y',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => '400c8a23-9583-4db2-b0f1-e7bcdabba0d6',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'U',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => '41303b78-e0f9-4e4f-b5e6-cb27ec570d41',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'O',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => '5f798ccf-aa87-43ef-9a00-591222f4c3b9',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'R',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => '6576cacb-0e82-4e65-9c7b-2a78476d1245',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'T',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => '695db438-130d-452b-949d-2206caad417b',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'L',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => '6f563876-bb01-4c2d-af77-ab6aac8d6d6a',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'D',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => '72d38f93-9eea-44d8-aa97-3f9d79b7a560',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'J',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => '86033ed0-fd5f-471a-9f5b-e4d90402bd51',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'N',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => '97089a30-a711-4add-8fea-946e28dd17f0',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'S',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => '9d1aeff0-da09-4777-9247-fc4d37ae5ea0',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'H',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => 'a25841d8-5871-4fcb-8a60-0fbe3aef50fe',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Q',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => 'ad732e5d-55e6-4afe-bb15-c1d18721b50a',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Z',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => 'ba183bb0-728b-4ceb-ba96-3d23f2d928f2',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'I',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => 'd085b48c-b01e-42f5-929e-7726d4c98812',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'W',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => 'e130a276-9be3-471a-bed3-8fb8cbbccbdd',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'P',
    'description' => '',
    'format' => 'uaqs_textual_content',
    'weight' => 0,
    'uuid' => 'ec0cb75b-0fc8-4e64-a908-3430e0ab0812',
    'vocabulary_machine_name' => 'uams_alphabet_list_filter',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  return $terms;
}
