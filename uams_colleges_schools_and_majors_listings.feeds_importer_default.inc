<?php
/**
 * @file
 * uams_colleges_schools_and_majors_listings.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function uams_colleges_schools_and_majors_listings_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'uams_colleges';
  $feeds_importer->config = array(
    'name' => 'Colleges',
    'description' => 'http://feed.uanow.org/colleges.json',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'json',
        'direct' => 0,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsJSONPathParser',
      'config' => array(
        'context' => '$....',
        'sources' => array(
          'jsonpath_parser:3' => 'UA_DS_PROG_VW.DESCR100',
          'jsonpath_parser:4' => 'UA_DS_PROG_VW.DESCR100',
          'jsonpath_parser:5' => 'UA_DS_PROG_VW.URL',
          'jsonpath_parser:7' => 'UA_DS_PROG_VW.DESCR100',
        ),
        'debug' => array(
          'options' => array(
            'context' => 0,
            'jsonpath_parser:3' => 0,
            'jsonpath_parser:4' => 0,
            'jsonpath_parser:5' => 0,
            'jsonpath_parser:7' => 0,
          ),
        ),
        'allow_override' => 0,
        'convert_four_byte' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsECKprocessor',
      'config' => array(
        'entity_type' => 'uams_feed_item',
        'bundle' => 'uams_feed_item_college',
        'expire' => '-1',
        'author' => 'uams_feeds',
        'authorize' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'jsonpath_parser:3',
            'target' => 'field_uams_alphabet_list_filter',
            'unique' => FALSE,
            'language' => 'und',
          ),
          1 => array(
            'source' => 'jsonpath_parser:4',
            'target' => 'field_uams_feed_item_link:title',
            'unique' => FALSE,
            'language' => 'und',
          ),
          2 => array(
            'source' => 'jsonpath_parser:5',
            'target' => 'field_uams_feed_item_link:url',
            'unique' => FALSE,
            'language' => 'und',
          ),
          3 => array(
            'source' => 'jsonpath_parser:7',
            'target' => 'title',
            'unique' => FALSE,
          ),
        ),
        'insert_new' => '1',
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'language' => 'und',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 1,
  );
  $export['uams_colleges'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'uams_majors';
  $feeds_importer->config = array(
    'name' => 'Majors',
    'description' => 'http://feed.uanow.org/majors2.json',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'json',
        'direct' => 0,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          'public' => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsJSONPathParser',
      'config' => array(
        'context' => '$....',
        'sources' => array(
          'jsonpath_parser:0' => 'UA_DS_MAJOR_DESCR',
          'jsonpath_parser:1' => 'UA_DS_MAJOR_DESCR',
          'jsonpath_parser:2' => 'URL',
          'jsonpath_parser:3' => 'UA_DS_MAJOR_DESCR',
        ),
        'debug' => array(
          'options' => array(
            'context' => 0,
            'jsonpath_parser:0' => 0,
            'jsonpath_parser:1' => 0,
            'jsonpath_parser:2' => 0,
            'jsonpath_parser:3' => 0,
          ),
        ),
        'allow_override' => 0,
        'convert_four_byte' => 1,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsECKprocessor',
      'config' => array(
        'entity_type' => 'uams_feed_item',
        'bundle' => 'uams_feed_item_major',
        'expire' => '-1',
        'author' => '',
        'authorize' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'jsonpath_parser:0',
            'target' => 'title',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'jsonpath_parser:1',
            'target' => 'field_uams_feed_item_link:title',
            'unique' => FALSE,
            'language' => 'und',
          ),
          2 => array(
            'source' => 'jsonpath_parser:2',
            'target' => 'field_uams_feed_item_link:url',
            'unique' => FALSE,
            'language' => 'und',
          ),
          3 => array(
            'source' => 'jsonpath_parser:3',
            'target' => 'field_uams_alphabet_list_filter',
            'unique' => FALSE,
            'language' => 'und',
          ),
        ),
        'insert_new' => '1',
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'language' => 'und',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 1,
  );
  $export['uams_majors'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'uams_schools';
  $feeds_importer->config = array(
    'name' => 'Schools',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'json',
        'direct' => 0,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          'public' => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsJSONPathParser',
      'config' => array(
        'context' => '*',
        'sources' => array(
          'jsonpath_parser:5' => 'URL',
          'jsonpath_parser:6' => 'School',
          'jsonpath_parser:7' => 'College',
        ),
        'debug' => array(
          'options' => array(
            'context' => 0,
            'jsonpath_parser:5' => 0,
            'jsonpath_parser:6' => 0,
            'jsonpath_parser:7' => 0,
          ),
        ),
        'allow_override' => 0,
        'convert_four_byte' => 1,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsECKprocessor',
      'config' => array(
        'entity_type' => 'uams_feed_item',
        'bundle' => 'uams_feed_item_school',
        'expire' => '-1',
        'author' => '',
        'authorize' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'jsonpath_parser:5',
            'target' => 'field_uams_feed_item_link:url',
            'unique' => FALSE,
            'language' => 'und',
          ),
          1 => array(
            'source' => 'jsonpath_parser:6',
            'target' => 'field_uams_feed_item_link:title',
            'unique' => FALSE,
            'language' => 'und',
          ),
          2 => array(
            'source' => 'jsonpath_parser:7',
            'target' => 'field_uams_feed_item_college:label',
            'unique' => FALSE,
            'language' => 'und',
          ),
        ),
        'insert_new' => '1',
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'language' => 'und',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 1,
  );
  $export['uams_schools'] = $feeds_importer;

  return $export;
}
