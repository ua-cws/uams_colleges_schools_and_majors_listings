<?php
/**
 * @file
 * uams_colleges_schools_and_majors_listings.features.defaultconfig.inc
 */

/**
 * Implements hook_defaultconfig_features().
 */
function uams_colleges_schools_and_majors_listings_defaultconfig_features() {
  return array(
    'uams_colleges_schools_and_majors_listings' => array(
      'strongarm' => 'strongarm',
    ),
  );
}

/**
 * Implements hook_defaultconfig_strongarm().
 */
function uams_colleges_schools_and_majors_listings_defaultconfig_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uuid_features_entity_taxonomy_term';
  $strongarm->value = array(
    'tags' => 0,
    'uaqs_research_areas' => 0,
    'uams_alphabet_list_filter' => 0,
  );
  $export['uuid_features_entity_taxonomy_term'] = $strongarm;

  return $export;
}
