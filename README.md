# Instructions for installation.

Download module from here into custom module folder:
`git clone
https://bitbucket.org/ua-cws/uams_colleges_schools_and_majors_listings.git`

Download feeds_eck_processor

`git clone git://git.drupal.org/sandbox/Andruxa/2301489.git feeds_eck_processor`

`cd feeds_eck_processor`

`curl https://www.drupal.org/files/issues/error-message-when-installed-with-feeds-version-7.x-2.0-beta2--2769609--1.patch | git apply -v`





Make sure you have this php library installedin `libraries/jsonpath`.

`https://jsonpath.googlecode.com/files/jsonpath-0.8.1.php`


Other module dependencies can be seen in the info file, but here is a list for
conveinience.

- ctools
- defaultconfig
- eck
- entityreference
- features
- feeds
- feeds_eck_processor
- feeds_jsonpath_parser
- feeds_tamper
- link
- options
- strongarm
- taxonomy
- uuid
- uuid_features
- views
- views_field_view

Handy drush command:
```
drush en -y ctools defaultconfig eck entityreference features feeds feeds_eck_processor feeds_jsonpath_parser feeds_tamper link options strongarm taxonomy uuid uuid_features views views_field_view

```
# Instructions for use.

To view entities you must go here: `/admin/structure/entity-type`

The Colleges and Schools view can be seen here: `/colleges-schools`

The Majors view can be seen here: `/majors`

Import content at this url: `/import`


# Instructions for importing.

Import content at this url: `/import`

Upload JSON files from the `import/data` in this repository, to importer [screencast below].

Screencast: http://take.ms/Ne1r5