<?php
/**
 * @file
 * uams_colleges_schools_and_majors_listings.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uams_colleges_schools_and_majors_listings_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'uams_feed_items_college_schools_list';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'eck_uams_feed_item';
  $view->human_name = 'Colleges and Schools alphabetical listing';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Colleges and Schools';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '0';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Footer: Global: Unfiltered text */
  $handler->display->display_options['footer']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['footer']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['footer']['area_text_custom']['field'] = 'area_text_custom';
  /* Field: Feed item: Alphabet filter */
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['id'] = 'field_uams_alphabet_list_filter';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['table'] = 'field_data_field_uams_alphabet_list_filter';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['field'] = 'field_uams_alphabet_list_filter';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['label'] = '';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['element_type'] = '0';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['settings'] = array(
    'field_multiple_limit' => '-1',
    'field_multiple_limit_offset' => '0',
  );
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['group_columns'] = array(
    'tid' => 'tid',
  );
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['group_rows'] = FALSE;
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['field_api_classes'] = TRUE;
  /* Contextual filter: COUNT(Alphabet filter) */
  $handler->display->display_options['arguments']['field_uams_alphabet_list_filter_tid']['id'] = 'field_uams_alphabet_list_filter_tid';
  $handler->display->display_options['arguments']['field_uams_alphabet_list_filter_tid']['table'] = 'field_data_field_uams_alphabet_list_filter';
  $handler->display->display_options['arguments']['field_uams_alphabet_list_filter_tid']['field'] = 'field_uams_alphabet_list_filter_tid';
  $handler->display->display_options['arguments']['field_uams_alphabet_list_filter_tid']['group_type'] = 'count';
  $handler->display->display_options['arguments']['field_uams_alphabet_list_filter_tid']['ui_name'] = 'Alphabet filter';
  $handler->display->display_options['arguments']['field_uams_alphabet_list_filter_tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_uams_alphabet_list_filter_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_uams_alphabet_list_filter_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_uams_alphabet_list_filter_tid']['summary_options']['items_per_page'] = '25';

  /* Display: Alphabet */
  $handler = $view->new_display('page', 'Alphabet', 'alphabet');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['row_class'] = 'alpha-list-level1__list-item';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['class'] = 'alpha-list-level1';
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Alphabetical Navigation */
  $handler->display->display_options['header']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['header']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['header']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['header']['area_text_custom']['ui_name'] = 'Alphabetical Navigation';
  $handler->display->display_options['header']['area_text_custom']['content'] = '<div class="local-header">
    <div class="container">
      <div class="l-1up">

  <ul class="local-nav">
    <li class="local-nav__list-item"><a class="local-nav__link local-nav__active" href="#">Colleges and Schools</a><span class="local-nav__active__arrow">
  </span></li>
    <li class="local-nav__list-item"><a class="local-nav__link" href="http://directory.arizona.edu/departments">Departments</a></li>
  </ul>

        <div class="l-main--left">

  <div class="alpha-nav">
    <ul class="alpha-nav__list">
      <li class="alpha-nav__list-item"><a class="alpha-nav__link has-content" href="#A">a</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link has-content" href="#B">b</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link no-content" href="#C">c</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link no-content" href="#D">d</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link has-content" href="#E">e</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link has-content" href="#F">f</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link no-content" href="#G">g</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link has-content" href="#H">h</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link no-content" href="#I">i</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link no-content" href="#J">j</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link no-content" href="#K">k</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link has-content" href="#L">l</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link has-content" href="#M">m</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link has-content" href="#N">n</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link no-content" href="#O">o</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link has-content" href="#P">p</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link no-content" href="#Q">q</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link no-content" href="#R">r</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link has-content" href="#S">s</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link no-content" href="#T">t</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link no-content" href="#U">u</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link no-content" href="#V">v</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link no-content" href="#W">w</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link no-content" href="#X">x</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link no-content" href="#Y">y</a></li>
      <li class="alpha-nav__list-item"><a class="alpha-nav__link has-content" href="#Z">z</a></li>
    </ul>
  </div>


        </div>
      </div>
    </div>
  </div>';
  /* Header: Global: Unfiltered text */
  $handler->display->display_options['header']['area_text_custom_1']['id'] = 'area_text_custom_1';
  $handler->display->display_options['header']['area_text_custom_1']['table'] = 'views';
  $handler->display->display_options['header']['area_text_custom_1']['field'] = 'area_text_custom';
  $handler->display->display_options['header']['area_text_custom_1']['label'] = 'No Results';
  $handler->display->display_options['header']['area_text_custom_1']['content'] = '<div class="alpha-list__no-results">
    <h2 class="rail--header">No results</h2>
    <p class="rail--cta-text">Try a different search query.</p>
  </div>';
  $handler->display->display_options['defaults']['footer'] = FALSE;
  /* Footer: Return to top button */
  $handler->display->display_options['footer']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['footer']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['footer']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['footer']['area_text_custom']['ui_name'] = 'Return to top button';
  $handler->display->display_options['footer']['area_text_custom']['content'] = '<a class="return-top" href="#"><span class="return-top__arrow"></span></a>';
  /* Footer: Global: Unfiltered text */
  $handler->display->display_options['footer']['area_text_custom_1']['id'] = 'area_text_custom_1';
  $handler->display->display_options['footer']['area_text_custom_1']['table'] = 'views';
  $handler->display->display_options['footer']['area_text_custom_1']['field'] = 'area_text_custom';
  $handler->display->display_options['footer']['area_text_custom_1']['label'] = 'Sidebar';
  $handler->display->display_options['footer']['area_text_custom_1']['content'] = '<div class="rail--content-wrapper">
    <h2 class="rail--header">Change the world one experience at a time</h2>
    <p class="rail--cta-text">Pursue your major with experiences beyond the classroom. Our 100% Engagement initiative gives every student the opportunity for real-world experiential learning.</p>
    <a class="btn--red-outline" href="http://ose.arizona.edu/100-engagement">Learn More</a>
  </div>';
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Alphabet filter */
  $handler->display->display_options['relationships']['field_uams_alphabet_list_filter_tid']['id'] = 'field_uams_alphabet_list_filter_tid';
  $handler->display->display_options['relationships']['field_uams_alphabet_list_filter_tid']['table'] = 'field_data_field_uams_alphabet_list_filter';
  $handler->display->display_options['relationships']['field_uams_alphabet_list_filter_tid']['field'] = 'field_uams_alphabet_list_filter_tid';
  $handler->display->display_options['relationships']['field_uams_alphabet_list_filter_tid']['ui_name'] = 'Alphabet filter';
  $handler->display->display_options['relationships']['field_uams_alphabet_list_filter_tid']['label'] = 'Alphabet filter';
  $handler->display->display_options['relationships']['field_uams_alphabet_list_filter_tid']['required'] = TRUE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Alphabet filter anchor */
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter_1']['id'] = 'field_uams_alphabet_list_filter_1';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter_1']['table'] = 'field_data_field_uams_alphabet_list_filter';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter_1']['field'] = 'field_uams_alphabet_list_filter';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter_1']['ui_name'] = 'Alphabet filter anchor';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter_1']['label'] = '';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter_1']['alter']['text'] = '<div id="[field_uams_alphabet_list_filter_1]"></div>';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter_1']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter_1']['settings'] = array(
    'field_multiple_limit' => '-1',
    'field_multiple_limit_offset' => '0',
  );
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter_1']['group_rows'] = FALSE;
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter_1']['delta_offset'] = '0';
  /* Field: Alphabet filter label */
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['id'] = 'field_uams_alphabet_list_filter';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['table'] = 'field_data_field_uams_alphabet_list_filter';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['field'] = 'field_uams_alphabet_list_filter';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['ui_name'] = 'Alphabet filter label';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['label'] = '';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['element_type'] = '0';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['empty_zero'] = TRUE;
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['settings'] = array(
    'field_multiple_limit' => '-1',
    'field_multiple_limit_offset' => '0',
  );
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['group_columns'] = array(
    'tid' => 'tid',
  );
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['group_rows'] = FALSE;
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_uams_alphabet_list_filter']['field_api_classes'] = TRUE;
  /* Field: Colleges View */
  $handler->display->display_options['fields']['view']['id'] = 'view';
  $handler->display->display_options['fields']['view']['table'] = 'views';
  $handler->display->display_options['fields']['view']['field'] = 'view';
  $handler->display->display_options['fields']['view']['ui_name'] = 'Colleges View';
  $handler->display->display_options['fields']['view']['label'] = '';
  $handler->display->display_options['fields']['view']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['view']['view'] = 'uams_feed_items_college_schools_list';
  $handler->display->display_options['fields']['view']['display'] = 'colleges';
  $handler->display->display_options['fields']['view']['arguments'] = '[%field_uams_alphabet_list_filter]';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Taxonomy term: Name */
  $handler->display->display_options['sorts']['name']['id'] = 'name';
  $handler->display->display_options['sorts']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['sorts']['name']['field'] = 'name';
  $handler->display->display_options['sorts']['name']['relationship'] = 'field_uams_alphabet_list_filter_tid';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Feed item: uams_feed_item type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_uams_feed_item';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'uams_feed_item_college' => 'uams_feed_item_college',
    'uams_feed_item_school' => 'uams_feed_item_school',
  );
  $handler->display->display_options['path'] = 'colleges-schools';

  /* Display: Colleges */
  $handler = $view->new_display('block', 'Colleges', 'colleges');
  $handler->display->display_options['defaults']['group_by'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['row_class'] = 'alpha-list-level2__list-item';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['class'] = 'alpha-list-level2';
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Feed item: Alphabet filter (field_uams_alphabet_list_filter) */
  $handler->display->display_options['relationships']['field_uams_alphabet_list_filter_tid']['id'] = 'field_uams_alphabet_list_filter_tid';
  $handler->display->display_options['relationships']['field_uams_alphabet_list_filter_tid']['table'] = 'field_data_field_uams_alphabet_list_filter';
  $handler->display->display_options['relationships']['field_uams_alphabet_list_filter_tid']['field'] = 'field_uams_alphabet_list_filter_tid';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Feed item: Link */
  $handler->display->display_options['fields']['field_uams_feed_item_link']['id'] = 'field_uams_feed_item_link';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['table'] = 'field_data_field_uams_feed_item_link';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['field'] = 'field_uams_feed_item_link';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['label'] = '';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_uams_feed_item_link']['alter']['path'] = '[field_uams_feed_item_link-url]';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['alter']['link_class'] = 'alpha-list-level2__link';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['element_type'] = '0';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_uams_feed_item_link']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_uams_feed_item_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['type'] = 'link_title_plain';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['group_column'] = 'url';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['group_columns'] = array(
    'entity_id' => 'entity_id',
    'url' => 'url',
    'title' => 'title',
  );
  /* Field: Feed item: Edit link */
  $handler->display->display_options['fields']['edit_link']['id'] = 'edit_link';
  $handler->display->display_options['fields']['edit_link']['table'] = 'eck_uams_feed_item';
  $handler->display->display_options['fields']['edit_link']['field'] = 'edit_link';
  $handler->display->display_options['fields']['edit_link']['label'] = '';
  $handler->display->display_options['fields']['edit_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['edit_link']['text'] = ' (edit)';
  /* Field: Feed item: Id */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'eck_uams_feed_item';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['label'] = '';
  $handler->display->display_options['fields']['id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['id']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['id']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['id']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['id']['separator'] = '';
  /* Field: Schools View */
  $handler->display->display_options['fields']['view']['id'] = 'view';
  $handler->display->display_options['fields']['view']['table'] = 'views';
  $handler->display->display_options['fields']['view']['field'] = 'view';
  $handler->display->display_options['fields']['view']['ui_name'] = 'Schools View';
  $handler->display->display_options['fields']['view']['label'] = '';
  $handler->display->display_options['fields']['view']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['view']['view'] = 'uams_feed_items_college_schools_list';
  $handler->display->display_options['fields']['view']['display'] = 'block_1';
  $handler->display->display_options['fields']['view']['arguments'] = '[!id]';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Feed item: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'eck_uams_feed_item';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Taxonomy term: Name */
  $handler->display->display_options['arguments']['name']['id'] = 'name';
  $handler->display->display_options['arguments']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['arguments']['name']['field'] = 'name';
  $handler->display->display_options['arguments']['name']['relationship'] = 'field_uams_alphabet_list_filter_tid';
  $handler->display->display_options['arguments']['name']['default_action'] = 'default';
  $handler->display->display_options['arguments']['name']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['name']['default_argument_options']['argument'] = 'a';
  $handler->display->display_options['arguments']['name']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['name']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['name']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['name']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['name']['validate']['type'] = 'taxonomy_term';
  $handler->display->display_options['arguments']['name']['validate_options']['vocabularies'] = array(
    'uams_alphabet_list_filter' => 'uams_alphabet_list_filter',
  );
  $handler->display->display_options['arguments']['name']['validate_options']['type'] = 'name';
  $handler->display->display_options['arguments']['name']['validate_options']['transform'] = TRUE;
  $handler->display->display_options['arguments']['name']['limit'] = '0';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Feed item: uams_feed_item type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_uams_feed_item';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'uams_feed_item_college' => 'uams_feed_item_college',
  );

  /* Display: Schools */
  $handler = $view->new_display('block', 'Schools', 'block_1');
  $handler->display->display_options['defaults']['group_by'] = FALSE;
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['row_class'] = 'alpha-list-level3__list-item';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['class'] = 'alpha-list-level3';
  $handler->display->display_options['style_options']['wrapper_class'] = '';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: College */
  $handler->display->display_options['relationships']['field_uams_feed_item_college_target_id']['id'] = 'field_uams_feed_item_college_target_id';
  $handler->display->display_options['relationships']['field_uams_feed_item_college_target_id']['table'] = 'field_data_field_uams_feed_item_college';
  $handler->display->display_options['relationships']['field_uams_feed_item_college_target_id']['field'] = 'field_uams_feed_item_college_target_id';
  $handler->display->display_options['relationships']['field_uams_feed_item_college_target_id']['ui_name'] = 'College';
  $handler->display->display_options['relationships']['field_uams_feed_item_college_target_id']['label'] = 'College';
  $handler->display->display_options['relationships']['field_uams_feed_item_college_target_id']['required'] = TRUE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Feed item: Link */
  $handler->display->display_options['fields']['field_uams_feed_item_link']['id'] = 'field_uams_feed_item_link';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['table'] = 'field_data_field_uams_feed_item_link';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['field'] = 'field_uams_feed_item_link';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['label'] = '';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_uams_feed_item_link']['alter']['path'] = '[field_uams_feed_item_link-url]';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['field_uams_feed_item_link']['alter']['link_class'] = 'alpha-list-level3__link';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['element_type'] = '0';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_uams_feed_item_link']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_uams_feed_item_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['type'] = 'link_title_plain';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['group_column'] = 'entity_id';
  $handler->display->display_options['fields']['field_uams_feed_item_link']['group_columns'] = array(
    'entity_id' => 'entity_id',
  );
  /* Field: Feed item: Edit link */
  $handler->display->display_options['fields']['edit_link']['id'] = 'edit_link';
  $handler->display->display_options['fields']['edit_link']['table'] = 'eck_uams_feed_item';
  $handler->display->display_options['fields']['edit_link']['field'] = 'edit_link';
  $handler->display->display_options['fields']['edit_link']['label'] = '';
  $handler->display->display_options['fields']['edit_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['edit_link']['text'] = ' (edit)';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Feed item: Id */
  $handler->display->display_options['arguments']['id']['id'] = 'id';
  $handler->display->display_options['arguments']['id']['table'] = 'eck_uams_feed_item';
  $handler->display->display_options['arguments']['id']['field'] = 'id';
  $handler->display->display_options['arguments']['id']['relationship'] = 'field_uams_feed_item_college_target_id';
  $handler->display->display_options['arguments']['id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['id']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['id']['validate']['type'] = 'numeric';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Feed item: uams_feed_item type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_uams_feed_item';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'uams_feed_item_school' => 'uams_feed_item_school',
  );

  $export['uams_feed_items_college_schools_list'] = $view;

  return $export;
}
