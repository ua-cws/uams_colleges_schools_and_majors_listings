<?php
/**
 * @file
 * uams_colleges_schools_and_majors_listings.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uams_colleges_schools_and_majors_listings_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uams_colleges_schools_and_majors_listings_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_eck_bundle_info().
 */
function uams_colleges_schools_and_majors_listings_eck_bundle_info() {
  $items = array(
    'uams_feed_item_uams_feed_item_college' => array(
      'machine_name' => 'uams_feed_item_uams_feed_item_college',
      'entity_type' => 'uams_feed_item',
      'name' => 'uams_feed_item_college',
      'label' => 'College',
      'config' => array(
        'managed_properties' => array(
          'title' => 'title',
          'created' => 0,
          'changed' => 0,
          'uams_feed_item_url' => 0,
        ),
      ),
    ),
    'uams_feed_item_uams_feed_item_major' => array(
      'machine_name' => 'uams_feed_item_uams_feed_item_major',
      'entity_type' => 'uams_feed_item',
      'name' => 'uams_feed_item_major',
      'label' => 'Major',
      'config' => array(),
    ),
    'uams_feed_item_uams_feed_item_school' => array(
      'machine_name' => 'uams_feed_item_uams_feed_item_school',
      'entity_type' => 'uams_feed_item',
      'name' => 'uams_feed_item_school',
      'label' => 'School',
      'config' => array(
        'managed_properties' => array(
          'uams_feed_item_name' => 'uams_feed_item_name',
          'created' => 0,
          'changed' => 0,
          'uams_feed_item_url' => 0,
        ),
      ),
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function uams_colleges_schools_and_majors_listings_eck_entity_type_info() {
  $items = array(
    'uams_feed_item' => array(
      'name' => 'uams_feed_item',
      'label' => 'Feed item',
      'properties' => array(
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
      ),
    ),
  );
  return $items;
}
